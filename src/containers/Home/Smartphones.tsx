import { useState } from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { Button } from '@mui/material';

interface Column {
    id: string;// 'name' | 'code' | 'population' | 'size' | 'density';
    label: string;
    minWidth?: number;
    align?: 'right';
    format?: (value: number) => string;
}

const columns: readonly Column[] = [
    { id: 'name', label: 'Name', minWidth: 170 },
    { id: 'model', label: 'Model', minWidth: 100 },
    {
        id: 'priceTag',
        label: 'priceTag',
        minWidth: 170,
        align: 'right',
        // format: (value: string) => value,
    },
    {
        id: 'salePrice',
        label: 'salePrice',
        minWidth: 170,
        align: 'right',
        // format: (value: number) => value.toLocaleString('en-US'),
    },
    {
        id: 'modelYear',
        label: 'modelYear',
        minWidth: 170,
        align: 'right',
        // format: (value: number) => value.toFixed(2),
    },
];

export default function Smartphones({
    smartphones = [], handleGetSmartphones, countSmartphones, handleConfiguration
}: {
    smartphones: object[], handleGetSmartphones: Function, countSmartphones: number, handleConfiguration: Function
}) {
    const [page, setPage] = useState(0);

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
        handleGetSmartphones({ page: newPage, limit: 10 });
    };

    return (
        <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ height: "80%" }}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {smartphones
                            // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row, index) => {
                                return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                                        {columns.map((column) => {
                                            const value = row[column.id];
                                            return (
                                                <TableCell key={column.id} align={column.align}>
                                                    {column.format && typeof value === 'number'
                                                        ? column.format(value)
                                                        : value}
                                                </TableCell>
                                            );
                                        })}
                                        <Button onClick={() => handleConfiguration(row)}>
                                            Configurar
                                        </Button>
                                    </TableRow>
                                );
                            })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                // rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={countSmartphones}
                rowsPerPage={smartphones.length}
                page={page}
                onPageChange={handleChangePage}
            // onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </Paper>
    );
}