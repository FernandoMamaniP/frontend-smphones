import Box from '@mui/material/Box';
import { TextField, Button } from '@mui/material';
import Modal from '@mui/material/Modal';
import FormControl from '@mui/material/FormControl';
import Input from '@mui/material/Input';
import InputLabel from '@mui/material/InputLabel';
import FormHelperText from '@mui/material/FormHelperText';
import { useEffect, useState } from 'react';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const initialState = {
    name: "",
    model: "",
    priceTag: "",
    salePrice: "",
    modelYear: "",
}
const FormSmartphone = ({ open, setOpen, handleSubmit, smartphoneSelected, handleDelete }:
    { open: boolean; setOpen: Function; handleSubmit: Function; smartphoneSelected: Partial<object> | undefined, handleDelete: Function | undefined }) => {
    const [inputs, setInputs] = useState(initialState);
    useEffect(() => {
        if (Boolean(smartphoneSelected)) setInputs(smartphoneSelected);
        if (!Boolean(smartphoneSelected)) setInputs(initialState);
    }, [smartphoneSelected])
    return (
        <Modal
            open={open}
            onClose={() => setOpen(false)}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <form autoComplete="off" onSubmit={(event) => {
                    event.preventDefault();
                    handleSubmit(inputs);
                }}>
                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                        <h2>Configuration</h2>
                        {Boolean(smartphoneSelected) && <Button onClick={handleDelete}>delete</Button>}
                    </div>
                    <TextField
                        label="name"
                        onChange={e => setInputs({ ...inputs, name: e.target.value })}
                        required
                        variant="outlined"
                        color="secondary"
                        // type="text"
                        sx={{ mb: 3 }}
                        fullWidth
                        value={inputs.name}
                    // error={emailError}
                    />
                    <TextField
                        label="model"
                        onChange={e => setInputs({ ...inputs, model: e.target.value })}
                        required
                        variant="outlined"
                        color="secondary"
                        // type="text"
                        sx={{ mb: 3 }}
                        fullWidth
                        value={inputs.model}
                    // error={emailError}
                    />
                    <TextField
                        label="priceTag"
                        onChange={e => setInputs({ ...inputs, priceTag: e.target.value })}
                        required
                        variant="outlined"
                        color="secondary"
                        // type="text"
                        sx={{ mb: 3 }}
                        fullWidth
                        value={inputs.priceTag}
                    // error={emailError}
                    />
                    <TextField
                        label="salePrice"
                        onChange={e => setInputs({ ...inputs, salePrice: e.target.value })}
                        required
                        variant="outlined"
                        color="secondary"
                        // type="text"
                        sx={{ mb: 3 }}
                        fullWidth
                        value={inputs.salePrice}
                    // error={emailError}
                    />
                    <TextField
                        label="modelYear"
                        onChange={e => setInputs({ ...inputs, modelYear: e.target.value })}
                        required
                        variant="outlined"
                        color="secondary"
                        // type="text"
                        sx={{ mb: 3 }}
                        fullWidth
                        value={inputs.modelYear}
                    // error={emailError}
                    />
                    <Button variant="outlined" color="primary" type="submit">{Boolean(smartphoneSelected) ? 'Modificar' : 'Crear'}</Button>
                </form>
                {/* <FormControl onSubmit={(data) => console.log(data)}>
                    <InputLabel htmlFor="my-input">Email address</InputLabel>
                    <Input id="my-input" aria-describedby="my-helper-text" />
                    <FormHelperText id="my-helper-text">We'll never share your email.</FormHelperText>
                    // <Button variant="outlined" color="secondary" type="text">Login</Button>
                </FormControl> */}
            </Box>
        </Modal >
    )
}
export default FormSmartphone;