import Button from '@mui/material/Button';
import Smartphones from './Smartphones';
import { useEffect, useState } from 'react';
import request, { deleteOptions, getOptions, patchOptions, postOptions } from '@/utils/request';
import FormSmartphone from './FormSmartphone';

const Home = () => {
    const [smartphones, setSmartphones] = useState([]);
    const [count, setCount] = useState(0);
    const [open, setOpen] = useState(false);
    const [openUpdate, setOpenUpdate] = useState(false);
    const [smartphoneSelected, setSmartphoneSelected] = useState<object>();
    const initialRequest = () => {
        handleGetSmartphones({});
        getCountSmartphones();
    }
    const getCountSmartphones = async () => {
        try {
            const url = `http://localhost:3100/smartphones?count=true`;
            const option = getOptions();
            const count = await request(url, option);
            setCount(count);
        } catch (error) {
            console.log("🚀 ~ file: index.tsx:18 ~ getCountSmartphones ~ error:", error)
        }
    }
    const handleGetSmartphones = async ({ page = 0, limit = 10 }) => {
        try {
            const url = `http://localhost:3100/smartphones?limit=${limit}&offset=${page * 10}`;
            const option = getOptions();
            const smartphones = await request(url, option);
            setSmartphones(smartphones);
        } catch (error) {
            console.log("🚀 ~ file: index.tsx:14 ~ handleGetSmartphones ~ error:", error)
        }
    }
    const handleUpdateSmartphone = async ({ id, ...newSmartphone }) => {
        try {
            const url = `http://localhost:3100/smartphones/${id}`;
            delete newSmartphone.audits;
            const option = patchOptions(newSmartphone);
            console.log({ ...newSmartphone, audits: [{ name: 'smartphone default' }] });
            await request(url, option);
            initialRequest();
            setOpenUpdate(false);
        } catch (error) {
            console.log("🚀 ~ file: index.tsx:43 ~ handleUpdateSmartphone ~ error:", error)
        }
    }
    const handleSubmit = async (newSmartphone: object) => {
        try {
            const url = 'http://localhost:3100/smartphones';
            const option = postOptions({ ...newSmartphone, audits: [{ name: 'smartphone default' }] });
            await request(url, option);
            initialRequest();
            setOpen(false);
        } catch (error) {
            console.log("🚀 ~ file: index.tsx:14 ~ handleGetSmartphones ~ error:", error)
        }
    }
    const handleDelete = async () => {
        try {
            const url = `http://localhost:3100/smartphones/${smartphoneSelected.id}`;
            const option = deleteOptions();
            await request(url, option);
        } catch (error) {
            console.log("🚀 ~ file: index.tsx:69 ~ handleDelete ~ error:", error)
        } finally {
            initialRequest();
            setOpenUpdate(false);
        }
    }
    const handleConfiguration = async (smartphone: object) => {
        await setSmartphoneSelected(smartphone);
        setOpenUpdate(true);
    }
    useEffect(() => {
        initialRequest();
    }, [])
    return <div>
        <Button onClick={() => { setOpen(true); setSmartphoneSelected(undefined); }} variant="contained">Agregar smartphone</Button>
        <Smartphones smartphones={smartphones} handleGetSmartphones={handleGetSmartphones} countSmartphones={count} handleConfiguration={handleConfiguration} />
        <FormSmartphone open={open} setOpen={setOpen} handleSubmit={handleSubmit} smartphoneSelected={undefined} handleDelete={undefined} />
        <FormSmartphone open={openUpdate} setOpen={setOpenUpdate} handleSubmit={handleUpdateSmartphone} smartphoneSelected={smartphoneSelected} handleDelete={handleDelete} />
    </div>;
}

export default Home;