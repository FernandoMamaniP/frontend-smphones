
const Main = ({ Component, pageProps }: { Component: React.ComponentClass, pageProps: object }) => {
    return (<Component {...pageProps} />);
}

export default Main;
