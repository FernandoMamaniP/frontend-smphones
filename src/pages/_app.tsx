import React from 'react';
import Main from '../containers/Main';

function MyApp({ Component, pageProps }: { Component: React.ComponentClass, pageProps: object }) {
  console.log('into re-render app')

  return (
    <Main pageProps={pageProps} Component={Component} />
  )
}

export default MyApp;
