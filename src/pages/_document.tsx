import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
    return (
        <Html>
            <Head>
                {/* <title>smphones</title> */}
                <meta name="Description" content="React next landing page" />
                <meta name="theme-color" content="#2563FF" />
                <meta
                    name="keywords"
                    content="React, React js, Next, Next js, Super fast next js landing, Modren landing, Next js landing"
                />
                <link
                    rel="stylesheet"
                    href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
                />

            </Head>
            <body>
                <Main />
                <NextScript />
            </body>
        </Html>
    )
}