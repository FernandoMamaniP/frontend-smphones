// import { expireSesion } from "./utils";

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
async function parseJSON(response: { status: number; json: any }) {
  return response.status === 204 ? "" : await response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(status: number) {
  if (status >= 200 && status < 300) return;

  // if (response.status == 401) {
  //   return Router.replace(`/ingreso?goBack=${btoa(Router.asPath)}`);
  // }

  throw new Error(status.toString());
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export default async function request(url: string, options: object) {
  try {
    let encode = await fetch(url, options);
    checkStatus(encode.status);

    return parseJSON(encode);
  } catch (err: any) {
    throw new Error(err);
  }
}

export function postOptions(body = {}, method = "POST") {
  return {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };
}

export function getOptions(method = "GET") {
  return {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  };
}

export function patchOptions(body = {}, method = "PATCH") {
  return {
    method,
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify(body),
  };
}

export function deleteOptions(body = {}, method = "DELETE") {
  return {
    method,
    headers: {
      Accept: "application/json",
    },
    body: JSON.stringify(body),
  };
}
